<?php

/**
 * Fetch the currency exchange rates for the requested currency combination.
 * Use tcmb.by as provider.
 *
 * Return an array with the array(target_currency_code => rate) combination.
 *
 * @param string $currency_code
 *   Source currency code.
 * @param array $target_currencies
 *   Array with the target currency codes.
 *
 * @return array
 *   Array with the array(target_currency_code => rate) combination.
 */
function commerce_multicurrency_try_exchange_rate_sync_provider_tcmb($currency_code, $target_currencies) {
  $data = cache_get(__FUNCTION__, 'cache');

  if (!$data) {
    $tcmb_rates = array();
    $xml = new DOMDocument();
    if (($xml->load('http://www.tcmb.gov.tr/kurlar/today.xml')) && @count($xml->getElementsByTagName("Currency"))) {
      foreach ($xml->getElementsByTagName("Currency") as $item) {
        $rate = !empty($item->getElementsByTagName("CrossRateUSD")->item(0)->nodeValue) ? (float) str_replace(',', '.', (string) $item->getElementsByTagName("CrossRateUSD")->item(0)->nodeValue) : (float) str_replace(",",".", (string) $item->getElementsByTagName("CrossRateOther")->item(0)->nodeValue);
        $tcmb_rates[(string) $item->getAttribute("CurrencyCode")] = empty($rate) ? 0 : 1 / $item->getElementsByTagName("ForexBuying")->item(0)->nodeValue / $rate;
      }
      cache_set(__FUNCTION__, $tcmb_rates, 'cache', time() + 3600);
    }
    else {
      watchdog(
        'commerce_multicurrency', 'Rate provider tcmb.gov.tr: Unable to fetch / process the currency data of @url',
        array('@url' => 'http://www.tcmb.gov.tr/kurlar/today.xml'),
        WATCHDOG_ERROR
      );
    }
  }
  else {
    $tcmb_rates = $data->data;
  }

  $rates = array();
  foreach ($target_currencies as $target_currency_code) {
    if ($currency_code == 'TRY' && isset($tcmb_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $tcmb_rates[$target_currency_code];
    }
    elseif (isset($tcmb_rates[$currency_code]) && $target_currency_code == 'TRY') {
      $rates[$target_currency_code] = 1 / $tcmb_rates[$currency_code];
    }
    elseif (isset($tcmb_rates[$currency_code]) && isset($tcmb_rates[$target_currency_code])) {
      $rates[$target_currency_code] = $tcmb_rates[$target_currency_code] / $tcmb_rates[$currency_code];
    }
  }

  return $rates;
}
