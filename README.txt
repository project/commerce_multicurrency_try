Turkish Republic Central Bank as currency exchange rate sync 
providers for Commerce multicurrency module

DEPENDENCIES
Commerce Multicurrency provider for TRY depends on the Commerce multicurrency 
module.

INSTALLATION
Install the module as usual.

CONFIGURATION
1. Select "Turkish Republic Central Bank" as sync provider on currency
   conversion settings page: admin/commerce/config/currency/conversion
2. Run cron or sync manually to synchronize the rates.
